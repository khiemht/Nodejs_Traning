import * as fs from "fs";

// Async function to read a text file
function readFileAsync(filePath: string): Promise<string> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, "utf8", (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
}

// Function to process the file data
function processFileData(data: string): number[] {
  const numbers: number[] = [];
  const regex = /\d+/g;
  const matches = data.match(regex);

  if (matches) {
    matches.forEach((match) => {
      numbers.push(parseInt(match));
    });
  }

  return numbers;
}

async function main(): Promise<void> {
  try {
    const filePath = "src/input.txt";
    const fileData: string = await readFileAsync(filePath);
    const numbers: number[] = processFileData(fileData);

    if (numbers.length > 0) {
      console.log("Numbers in the file:");
      numbers.forEach((number) => {
        console.log(number);
      });
    } else {
      console.log("No numbers found in the file.");
    }
  } catch (error) {
    console.log("An error occurred:", error);
  }
}

main().catch((error) => {
  console.log("An error occurred:", error);
});
