import * as readline from "readline";
import * as fs from "fs";

// Function to process the file data
function processFileData(data: string): number[] {
  const numbers: number[] = [];
  const regex = /\d+/g;
  const matches = data.match(regex);

  if (matches) {
    matches.forEach((match) => {
      numbers.push(parseInt(match));
    });
  }

  return numbers;
}

async function main(): Promise<void> {
  try {
    const filePath = "src/input.txt";
    const fileStream = fs.createReadStream(filePath, "utf8");
    const rl = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity,
    });

    const lines: string[] = [];

    rl.on("line", (line) => {
      lines.push(line);
    });

    rl.on("close", () => {
      const fileData = lines.join("\n");
      const numbers: number[] = processFileData(fileData);

      if (numbers.length > 0) {
        console.log("Numbers in the file:");
        numbers.forEach((number) => {
          console.log(number);
        });
      } else {
        console.log("No numbers found in the file.");
      }
    });
  } catch (error) {
    console.log("An error occurred:", error);
  }
}

main().catch((error) => {
  console.log("An error occurred:", error);
});
