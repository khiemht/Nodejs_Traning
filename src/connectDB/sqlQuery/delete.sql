SELECT c.customerName, e.firstName, e.lastName 

FROM customers c 

JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber; 