 SELECT * 

FROM customers 

WHERE EXISTS (SELECT * 

              FROM employees 

WHERE customers.salesRepEmployeeNumber = employees.employeeNumber); 