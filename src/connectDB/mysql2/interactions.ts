import connection from "./connection";

// CREATE
async function createCustomer(data) {
  try {
    const query = "INSERT INTO customers SET ?";
    const result = await (await connection).execute(query, data);
    console.log("Customer created: ", result);
  } catch (error) {
    console.error("Create Customer Error: ", error);
  }
}

// READ
async function readCustomers() {
  try {
    const query = "SELECT * FROM customers";
    const [rows, fields] = await (await connection).execute(query);
    console.log("Customer information: ", rows);
  } catch (error) {
    console.error("Read Customers Error: ", error);
  }
}

// UPDATE
async function updateCustomer(id, data) {
  try {
    const query =
      "UPDATE customers SET contactLastName = 'Nguyen', contactFirstName = 'Jane' WHERE customerNumber = 103";
    const result = await (await connection).execute(query, [data, id]);
    console.log("Customer updated: ", result);
  } catch (error) {
    console.error("Update Customer Error: ", error);
  }
}

// DELETE
async function deleteCustomer(id) {
  try {
    const query = "DELETE FROM customers WHERE customerNumber = 112";
    const result = await (await connection).execute(query, id);
    console.log("Customer deleted: ", result);
  } catch (error) {
    console.error("Delete Customer Error: ", error);
  }
}
