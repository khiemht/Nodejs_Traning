import knex, { Knex } from "knex";

const connection: Knex = knex({
  client: "mysql",
  connection: {
    host: "localhost",
    user: "root",
    password: "123123123",
    database: "mydatabase",
  },
});

export default connection;
