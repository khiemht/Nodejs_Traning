import connection from "./connection";

// Create
async function createCustomer(data) {
  try {
    const customer = await connection("customers").insert(data);
    console.log("Customer created: ", customer);
  } catch (error) {
    console.error("Insert Error: ", error);
  }
}

// Read
async function readCustomers() {
  try {
    const customers = await connection.select().from("customers");
    console.log("Customer information: ", customers);
  } catch (error) {
    console.error("Query Error: ", error);
  }
}

// Update
async function updateCustomer(id, data) {
  try {
    const customer = await connection("customers").where('id', id).update(data);
    console.log("Customer updated: ", customer);
  } catch (error) {
    console.error("Update Error: ", error);
  }
}

// Delete
async function deleteCustomer(id) {
  try {
    const customer = await connection("customers").where('id', id).del();
    console.log("Customer removed: ", customer);
  } catch (error) {
    console.error("Delete Error: ", error);
  }
}

createCustomer({name: 'New Customer', email: 'new@email.com'});
readCustomers();
updateCustomer(1, {name: 'Updated Customer'});
deleteCustomer(1);

connection.destroy();