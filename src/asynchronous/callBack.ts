// src/callbacksFiles.ts
import fs from "fs";

function processFile(
  filename: string,
  callback: (error: Error | null, data?: string) => void
) {
  fs.readFile(filename, "utf8", (err, data) => {
    if (err) {
      callback(err);
    } else {
      console.log(`File data: ${data}`);
      callback(null);
    }
  });
}

const files = [
  "src/data/input1.txt",
  "src/data/input2.txt",
  "srcs/data/input3.txt",
];

files.forEach((file) => {
  processFile(file, (err) => {
    if (err) {
      console.error(`Error processing file ${file}: ${err.message}`);
    } else {
      console.log(`File ${file} processed.`);
    }
  });
});
