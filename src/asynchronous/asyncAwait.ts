import fs from "fs";
import util from "util";

const readFile = util.promisify(fs.readFile);

async function processFile(filename: string) {
  try {
    const data = await readFile(filename, "utf8");
    console.log(`File data: ${data}`);
  } catch (err) {
    if (err instanceof Error) {
      console.error(`Error processing file: ${err.message}`);
    }
  }
}

const files = [
  "src/data/input1.txt",
  "src/data/input2.txt",
  "src/data/input3.txt",
];

async function processFiles() {
  for (const file of files) {
    await processFile(file);
  }
}

processFiles();
