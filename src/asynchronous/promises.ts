import fs from "fs";
import util from "util";

// Converts fs.readFile into a function returning a promise
const readFile = util.promisify(fs.readFile);

function processFile(filename: string) {
  return readFile(filename, "utf8").then((data) => {
    console.log(`File data: ${data}`);
  });
}

// Array to list files path
const files = [
  "src/data/input1.txt",
  "src/data/input2.txt",
  "srcs/data/input3.txt",
];

Promise.all(files.map(processFile))
  .then(() => console.log("All files processed."))
  .catch((err) => console.error(`Error processing files: ${err}`));
