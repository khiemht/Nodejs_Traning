import * as fs from "fs";

export async function readFileAsync(filePath: string): Promise<string> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, "utf8", (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
}

export async function saveFileAsync(
  filePath: string,
  data: string
): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, data, "utf8", (error) => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });
}
