import { readFileAsync, saveFileAsync } from "./fileUtils";

async function processFile(filePath: string): Promise<void> {
  try {
    const fileData = await readFileAsync(filePath);
    console.log("File content:", fileData);

    // Process the file data 

    const processedData = fileData.toUpperCase();
    console.log("Processed data:", processedData);

    // Save the processed data to a new file
    await saveFileAsync("output.txt", processedData);
    console.log("Data saved to output.txt");
  } catch (error) {
    console.log("An error occurred:", error);
  }
}
