import fs from "fs";
import path from "path";

async function readFiles(directory: string): Promise<string[]> {
  const files = await fs.promises.readdir(directory);
  return files.map((file) => path.join(directory, file));
}

async function processFiles() {
  try {
    const files = await readFiles("src/data/");
    for (const file of files) {
      const data = await fs.promises.readFile(file, "utf-8");
      console.log(`File: ${file}\n${data}\n`);
    }
  } catch (e) {
    console.error(`Error processing files: ${e.message}`);
  }
}

processFiles();
