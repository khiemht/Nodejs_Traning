import express from "express";
import fs from "fs";

const app = express();

app.get("/read/:file", async (req, res, next) => {
  const { file } = req.params;
  fs.readFile(`src/data/${file}`, "utf8", function (err, data) {
    if (err) {
      next(err);
    } else {
      res.send(data);
    }
  });
});

app.use((error, req, res, next) => {
  console.error(error.message);
  res.status(500).send("Internal Server Error");
});

app.listen(3000);
