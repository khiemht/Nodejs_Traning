import EventEmitter from "events";
import fs from "fs";

class WithTime extends EventEmitter {
  execute(asyncFunc, ...args) {
    console.time("execute");
    this.emit("begin");
    asyncFunc(...args, (err, data) => {
      if (err) {
        return this.emit("error", err);
      }
      this.emit("data", data);
      console.timeEnd("execute");
      this.emit("end");
    });
  }
}

const withTime = new WithTime();

withTime.on("begin", () => console.log("About to execute"));
withTime.on("end", () => console.log("Done with execute"));
withTime.on("error", (err) => console.error(err.message));

// read multiple files
["src/data/input1.txt", "src/data/input2.txt", "srcs/data/input3.txt"].forEach(
  (file) => {
    withTime.execute(fs.readFile, `./data/${file}`);
  }
);
