import util from "util";
import fs from "fs";

const readFile = util.promisify(fs.readFile);

async function readAndProcessFiles(files: string[]) {
  try {
    const fileDataPromises = files.map((file) => readFile(`./data/${file}`));
    const fileData = await Promise.all(fileDataPromises);
    // process file data
    console.log(fileData);
  } catch (e) {
    console.error(`Error reading files: ${e.message}`);
  }
}

readAndProcessFiles([
  "src/data/input1.txt",
  "src/data/input2.txt",
  "srcs/data/input3.txt",
]);
